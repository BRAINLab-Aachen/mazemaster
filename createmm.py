# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 16:19:25 2019

@author: bexter
"""
#----------------------------------------------------------
# File objects.py
#----------------------------------------------------------
import bpy
from mathutils import Vector
import os
import sys
sys.path.append(bpy.path.abspath("//modules"))
sys.path.append(bpy.path.abspath("//modules/GUI"))
from MM_config import Config
from MM_maze import Maze


#-----------------------------
print("CreateMaze")

#delete previous Maze
bpy.data.objects["Mouse"].game.properties['Quit'].value=False
bpy.data.objects["Mouse"].game.properties['NICount'].value=0  
#walls
bpy.ops.object.select_pattern(pattern="MazeWall****", case_sensitive=False, 
                              extend=False)
bpy.ops.object.delete(use_global=False)
#sensors
bpy.ops.object.select_pattern(pattern="Sensor****", case_sensitive=False, 
                              extend=False)
bpy.ops.object.delete(use_global=False)
#False Walls
bpy.ops.object.select_pattern(pattern="FalseWall****", case_sensitive=False, 
                              extend=False)
bpy.ops.object.delete(use_global=False)

#position rewards
for i in range(20):
    if i<10:
        bpy.data.objects[("Reward.0"+str(i))].location = (0,0,-200) 
    else:
        bpy.data.objects[("Reward."+str(i))].location = (0,0,-200)    
for i in range(5):
    bpy.data.objects[("Cue"+str(i))].location = (0,0,-150)   
    bpy.data.objects[("Cueb"+str(i))].location = (0,0,-150) 
    Cue = bpy.data.objects["Cue"+str(i)]

#-----------------------------

SensorNo = 0
FalseWallNo=0
NumGratingWall=0

#Load config
config = Config(bpy.path.abspath('//config.ini'))

space_bge = float(config.get("conf", "sizefactorBlender"))
space_pyt = float(config.get("conf", "sizefactorPython"))

#get the resizer
resizer = space_bge/space_pyt

bpy.data.objects["Camera"].game.properties['resizer'].value=resizer
bpy.data.objects["Mouse"].game.properties['NICount'].value=0
bpy.data.objects["Mouse"].game.properties['TrialNumber'].value=0


#read mouseIDs
MouseID1 = float(config.get('conf', 'mouseid1'))
MouseID2 = float(config.get('conf', 'mouseid2'))

#read imagesize ratios for walls     
WallImageRatioL= int(config.get('conf', 'imgfactl')) 
WallImageRatioR= int(config.get('conf', 'imgfactr')) 
 
 
def LoadMaterials():
    D = bpy.data     
    
    
    #load ceiling
    realpath = (bpy.path.abspath(
            "//Data\wall_textures\maze_textures\_temp\ceiling.png"))
    try:
        img = bpy.data.images.load(realpath)
    except:
        raise NameError("Cannot load image %s" % realpath)
    
    bpy.data.textures["ceiling"].image = img  
                
    for i in range(10):
        D.objects[('Cue'+str(i))].data.materials.clear()
        D.objects[('Cueb'+str(i))].data.materials.clear()
        CueTextPath = bpy.path.abspath(
                "//Data\wall_textures\cues\cue_"+str(i)+".png")
        
        if os.path.isfile(CueTextPath):
            try:
                img = bpy.data.images.load(CueTextPath)
            except:
                raise NameError("Cannot load image %s" % CueTextPath)
            
            # Create image texture from image
            CueTex = bpy.data.textures.new('CueTexture', type = 'IMAGE')
            CueTex.image = img
            print(CueTextPath)
            # Create material
            matCue = D.materials.new('CueMaterial'+str(i))
         
            #disable backface culling
            matCue.game_settings.use_backface_culling = False
            
            # Add texture slot for color texture
            mtex = matCue.texture_slots.add()
            mtex.texture = CueTex
            mtex.texture_coords = 'UV'
            mtex.use_map_color_diffuse = True 
            mtex.use_map_color_emission = True 
            mtex.emission_color_factor = 0.5

            mtex.mapping = 'FLAT' 
            
            mtex.uv_layer = 'UVMap'

            D.objects[('Cue'+str(i))].data.materials.append(matCue)  
            
        CueTextPath = bpy.path.abspath(
                "//Master Programm\Texture_"+str(i)+"_mirr.jpg")
        CueTextPath = bpy.path.abspath(
                "//Data\wall_textures\cues\cue_"+str(i)+"_mirr.png")

        if os.path.isfile(CueTextPath):
            try:
                img = bpy.data.images.load(CueTextPath)
            except:
                raise NameError("Cannot load image %s" % CueTextPath)
            
            # Create image texture from image
            CueTex = bpy.data.textures.new('CueTextureb', type = 'IMAGE')
            CueTex.image = img
            
            # Create material
            matCue = D.materials.new('CueMaterialb'+str(i))
         
            #disable backface culling
            matCue.game_settings.use_backface_culling = False
            
            # Add texture slot for color texture
            mtex = matCue.texture_slots.add()
            mtex.texture = CueTex
            mtex.texture_coords = 'UV'
            mtex.use_map_color_diffuse = True 
            mtex.use_map_color_emission = True 
            mtex.emission_color_factor = 0.5
            
            mtex.mapping = 'FLAT' 
            
            mtex.uv_layer = 'UVMap'

            D.objects[('Cueb'+str(i))].data.materials.append(matCue)


def deleteOldMaterials():
    #delete old materials
    for material in bpy.data.materials:
        if material.name.startswith('WallMat') or \
        material.name.startswith('CueMaterial') or \
        material.name.startswith('SensorMat') or \
        material.name.startswith('CueMaterialb') :
            if not material.users:
                bpy.data.materials.remove(material, do_unlink=True) 
 
    #delete old textures        
    for texture in bpy.data.textures:
        if texture.name.startswith('WallTex') or \
        texture.name.startswith('CueTexture') or \
        texture.name.startswith('CueTextureb'):
            if not texture.users:
                #texture.user_clear()
                bpy.data.textures.remove(texture, do_unlink=True)  
 
def createMeshFromOperator(name, origin, verts, faces,type):
    bpy.ops.object.add(
        type='MESH', 
        enter_editmode=False,
        location=origin)
        
    ob = bpy.context.object
    #ob.group_link(group='Walls')
    ob.name = name
    ob.show_name = True
    me = ob.data
    me.name = name+'Mesh'

    # Create mesh from given verts, faces.
    me.from_pydata(verts, [], faces)
    
    bpy.ops.object.editmode_toggle()
    bpy.ops.uv.unwrap(method='ANGLE_BASED', fill_holes=True, 
                      correct_aspect=True)

    bpy.ops.object.editmode_toggle()

    me.show_double_sided = True
    # Update mesh with new data
    me.update()    
    # Set object mode
    bpy.ops.object.mode_set(mode='OBJECT')
    
    #Material

    realpath = (bpy.path.abspath(
            "//Data\wall_textures\maze_textures\_temp\wall.png"))
    try:
        img = bpy.data.images.load(realpath)
    except:
        raise NameError("Cannot load image %s" % realpath)
 
    # Create image texture from image
    
 
    if type==0:
        # Create material
        mat = bpy.data.materials.new('WallMat')
        #Property
        bpy.ops.object.game_property_new(name="Wall")

             
    elif type==1:
        mat = bpy.data.materials.new('SensorMat')
        mat.use_transparency = True
        mat.alpha = 1.0
        mat.game_settings.invisible = True
        
        #Property
        bpy.ops.object.game_property_new(name="Sensor")
        ob.game.use_ghost = True
        img = bpy.data.images.load(bpy.path.abspath("//Data/sensor.jpg"))
        
    elif type==2: #False Wall
        # Create material
        mat = bpy.data.materials.new('WallMat')
        #Property
        bpy.ops.object.game_property_new(name="Wall")
                
    cTex = bpy.data.textures.new('WallTex', type = 'IMAGE')
    cTex.image = img  
    #disable backface culling
    mat.game_settings.use_backface_culling = False
    
    # Add texture slot for color texture
    mtex = mat.texture_slots.add()
    mtex.texture = cTex
    mtex.texture_coords = 'UV'
    mtex.use_map_color_diffuse = True 
    mtex.use_map_color_emission = True 
    mtex.emission_color_factor = 0.5
    #mtex.use_map_density = True 
    mtex.mapping = 'CUBE' 
    
    mtex.uv_layer = 'UVMap'
    
   
    #normalize texture to lenght of wall
    WallLength = ob.dimensions[1] + ob.dimensions[0]
    #scale texture to lenght
    
    mtex.scale = (WallLength/(4.0*WallImageRatioL),WallLength/4.0,1)
    
    me.materials.append(mat)     

    
    #Property
    #bpy.ops.object.game_property_new(name="Wall")
    ob.game.use_collision_bounds = True
    ob.game.collision_bounds_type = 'CONVEX_HULL'
    ob.game.collision_margin = 0.9
    return ob

def createWall(Ox,Oy,x,y,type, name = '', wallNo=None, heigth=4):   #type 0=wall, 1=sensor
    global SensorNo,NumGratingWall,FalseWallNo
    #if type ==2: #False Wall -place wall higher
    #    Oz=3
    #else:    
    Oz=0      
    try:
        if heigth>0 and heigth<100:
            z=float(heigth)*4.0
        else:
            z=4
    except:
        z=4        
            
    Origin = Vector((0,0,0))
   
    verts = ((Ox,Oy,Oz), (x,y,Oz), (Ox,Oy,z), (x,y,z))
    faces = ((1,2,0), (1,2,3))
    if type==0: #wall   
        createMeshFromOperator('MazeWall'+str(wallNo), Origin, 
                                      verts, faces,0)     
        NumGratingWall=NumGratingWall+1
    elif type ==1: #sensor
        if name =='':
            if SensorNo<10:
                createMeshFromOperator(('Sensor'+"0"+str(SensorNo)), 
                                                Origin, verts, faces,1) 
            else:
                createMeshFromOperator(('Sensor'+str(SensorNo)), 
                                                Origin, verts, faces,1)     
            SensorNo=SensorNo+1   
        else:
            createMeshFromOperator(('Sensor'+str(name)), 
                                            Origin, verts, faces,1)     
    elif type ==2: #False Wall
        if FalseWallNo<10:
            createMeshFromOperator(('FalseWall'+"0" + \
                                    str(FalseWallNo)), Origin, verts, faces,2) 
        else:
            createMeshFromOperator(('FalseWall'+str(FalseWallNo)), 
                                               Origin, verts, faces,2)     
           
        
        locate=bpy.data.objects['FalseWall'+"0"+str(FalseWallNo)].location
        bpy.data.objects['FalseWall'+"0"+str(FalseWallNo)].location = (
                (locate[0],locate[1],locate[2]+100)) 
        FalseWallNo=FalseWallNo+1   
    return

def createMaze(config):
    import ast
    global RewardNo,GratingsTrial
    print("create maze")
    #create maze object 
    maze = Maze(0, 0, mazefile=bpy.path.abspath('//InputMaze.maze'))
    
    #some variables
    WallCoords = [0 for i in range(4)] 
    bpy.data.objects["Mouse"].game.properties['Gratings'].value=False
    GratingsTrial = False
    
    deleteOldMaterials()
    LoadMaterials()
    
    #Enable Different Gratings for linear track
    if maze.get('settings','gratings') == 'True':
       GratingsTrial = True
       bpy.data.objects["Mouse"].game.properties['Gratings'].value=True
               
    #make reward invisible/visible
    try:
        if maze.get('settings','reward_visible') == 'True' or \
        maze.get('settings','reward_visible') == True:
            for i in range(20):    
                if i <10:
                    _rew = "Reward.0" + str(i)
                else:
                    _rew = "Reward." + str(i)
                bpy.data.objects[_rew].hide_render = False
        else:
            for i in range(20):    
                if i <10:
                    _rew = "Reward.0" + str(i)
                else:
                    _rew = "Reward." + str(i)
                bpy.data.objects[_rew].hide_render = True
    except:
        pass
    
    # build Walls
    for wallNo in maze.data['Walls']:  
        
        _wall = ast.literal_eval(maze.data.get('Walls', wallNo))
             
        #to not get a bug with the texture: 
        #build walls from y: greater value to smaller value, 
        #x: smaller value to greater value
        
        if _wall[2]==_wall[0] or _wall[1]==_wall[3]:#straight line
            if _wall[0]>_wall[2]:   #X-axis
                WallCoords[0]=float(_wall[2])*resizer
                WallCoords[2]=float(_wall[0])*resizer
            else:
                WallCoords[2]=float(_wall[2])*resizer
                WallCoords[0]=float(_wall[0])*resizer
                
            if float(_wall[1])>float(_wall[3]):   #Y-axis
                WallCoords[1]=float(_wall[1])*resizer
                WallCoords[3]=float(_wall[3])*resizer
            else:
                WallCoords[1]=float(_wall[3])*resizer
                WallCoords[3]=float(_wall[1]) *resizer    
        else: #diagonal line                  
            WallCoords[0]=float(_wall[0])*resizer
            WallCoords[1]=float(_wall[1])*resizer
            WallCoords[2]=float(_wall[2])*resizer
            WallCoords[3]=float(_wall[3])*resizer
        
        createWall(WallCoords[0],-WallCoords[1],WallCoords[2],-WallCoords[3], 0, wallNo = wallNo, heigth=float(config.get("conf", "wallHeigth")))


    #Reward
    for rewNo in maze.data['Rewards']:
        _entry = ast.literal_eval(maze.data.get('Rewards',rewNo))
        
        bpy.data.objects[("Reward.0"+str(rewNo))].location = (
                float(_entry[0])*resizer,-float(_entry[1])*resizer,1.2)    
    
    #Start position     
    _entry = ast.literal_eval(maze.data.get('Positions','start'))
   
    #Set Position for camera and "mouse"
    bpy.data.objects["Mouse"].location = (_entry[0]*resizer,-_entry[1] * \
                    resizer,1)  
    
    #sensor position   
    for sensorNo in maze.data.options('Sensors'):#maze.data['Sensors']:
        
        _entry = ast.literal_eval(maze.data.get('Sensors',sensorNo))

        #create sensor
        if len(str(sensorNo))==1:
            _name = '0'+str(sensorNo)
        else:
            _name = str(sensorNo)
        createWall(float(_entry[0])*resizer,-float(_entry[1])*resizer,
                   float(_entry[2])*resizer,-float(_entry[3])*resizer, 
                   1, name = _name)

    #false wall position   
    for falwallNo in maze.data['False Walls']:
        _entry = ast.literal_eval(maze.data.get('False Walls',falwallNo))

        #create false wall
        createWall(float(_entry[0])*resizer,-float(_entry[1])*resizer,
                   float(_entry[2])*resizer,-float(_entry[3])*resizer,2)

                  
    bpy.data.objects["Ceiling"].location = (0,0,6)    
    

if __name__ == "__main__":
    createMaze(config)
    if config.get('conf', 'monitors') == 'semicircle':
        bpy.data.scenes["Scene"].game_settings.stereo = 'DOME'
    else:
        bpy.data.scenes["Scene"].game_settings.stereo = 'NONE'
    
    bpy.ops.wm.save_as_mainfile(filepath=bpy.data.filepath)



 

           
  
